package racetrack

import org.grails.datastore.gorm.finders.MethodExpression.InList;

class User {
	
	String login
	String password
	String role="user"

    static constraints = {
		login(blank: false, nullable: false, unique: true)
		password(blank: false, password: true)
		role(InList: ["admin", "user"])
    }
	
	String toString(){
		login
	}
}
