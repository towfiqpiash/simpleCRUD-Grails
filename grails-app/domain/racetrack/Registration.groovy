package racetrack

class Registration {
	
	Boolean paid
	Date dateCreated
	//Date lastUpdated
	
//	static mapping = {
//		autoTimestamp = false	// to prevent automatic timestamp
//	}
	
    static constraints = {
		race()
		runner()
		paid()
		dateCreated()
    }
	
	static belongsTo = [race:Race, runner:Runner]
}
