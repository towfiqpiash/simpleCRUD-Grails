package racetrack



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(RaceController)
class RaceControllerTests {

    void testHello(){
		controller.hello()
		
		assert response.text == 'hello'
	}
	
	void testIndex(){
		controller.index()
		
		assert response.redirectUrl == '/race/hello2'
	}
}
